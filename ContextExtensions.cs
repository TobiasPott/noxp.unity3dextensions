﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ContextExtensions
{
    public const string Edit = "Edit/";
    public const string Menu = "Tools/";
    public const string SubSelection = "Selection/";


    /// <summary>
    /// wraps the "Find References In Scene" context menu item from the project view to the Shift + Alt + F shortcut
    /// </summary>
    [MenuItem(Menu + "Find References in Scene (Quick) #&f")]
    public static void ExecFindReferencesInScene()
    {
        EditorApplication.ExecuteMenuItem("Assets/Find References In Scene");
        EditorUtility.ClearProgressBar();
    }


    /// <summary>
    /// sorts the children of the selected object(s) alphabetically (ascending)
    /// </summary>
    [MenuItem(Menu + "Sort children (a-z)")]
    public static void SortChildrenAlphabeticallyASC()
    {
        SortChildrenAlphabetically(false);
    }
    /// <summary>
    /// sorts the children of the selected object(s) alphabetically (descending)
    /// </summary>
    [MenuItem(Menu + "Sort Children (z-a)")]
    public static void SortChildrenAlphabeticallyDESC()
    {
        SortChildrenAlphabetically(true);
    }
    private static void SortChildrenAlphabetically(bool desc)
    {
        foreach (GameObject go in Selection.gameObjects)
        {
            List<Transform> children = new List<Transform>();
            for (int i = 0; i < go.transform.childCount; i++)
                children.Add(go.transform.GetChild(i));
            children.Sort(CompareTransformByName);
            if (desc)
                children.Reverse();

            for (int i = 0; i < children.Count; i++)
                children[i].SetAsLastSibling();
        }
    }
    private static int CompareTransformByName(Transform x, Transform y)
    {
        return x.gameObject.name.CompareTo(y.gameObject.name);
    }

    /// <summary>
    /// selects all child objects of the currently selected objects
    /// </summary>
    [MenuItem(Menu + SubSelection + "Select Children (direct)")]
    public static void SelectChildren()
    {
        List<GameObject> newSelection = new List<GameObject>();
        foreach (GameObject sel in Selection.gameObjects)
        {
            for (int i = 0; i < sel.transform.childCount; i++)
                newSelection.Add(sel.transform.GetChild(i).gameObject);
        }
        if (newSelection.Count > 0)
            Selection.objects = newSelection.ToArray();
    }

    /// <summary>
    /// selects all child objects of the currently selected objects
    /// </summary>
    [MenuItem(Menu + SubSelection + "Invert On Hierarchy Level")]
    public static void InvertSelectionOnHierarchyLevel()
    {
        List<GameObject> parents = new List<GameObject>();
        bool includeTopLevel = false;
        foreach (GameObject sel in Selection.gameObjects)
        {
            if (sel.transform.parent == null)
            {
                includeTopLevel = true;
                continue;
            }
            //if (!parents.Contains(sel.transform.parent.gameObject))
            parents.Add(sel.transform.parent.gameObject);
        }
        IEnumerable<GameObject> dist = parents.Distinct();

        List<GameObject> newSelection = new List<GameObject>();
        foreach (GameObject parent in parents)
        {
            Transform pTfs = parent.transform;
            for (int i = 0; i < pTfs.childCount; i++)
            {
                if (Array.IndexOf(Selection.gameObjects, pTfs.GetChild(i).gameObject) == -1)
                    newSelection.Add(pTfs.GetChild(i).gameObject);
            }
        }
        // invert selection on top level of the hierarchy
        if (includeTopLevel)
        {
            Scene scene = SceneManager.GetActiveScene();
            parents.Clear();
            scene.GetRootGameObjects(parents);

            foreach (GameObject parent in parents)
            {
                Transform pTfs = parent.transform;
                if (Array.IndexOf(Selection.gameObjects, pTfs.gameObject) == -1)
                    newSelection.Add(pTfs.gameObject);
            }
        }

        if (newSelection.Count > 0)
        {
            Selection.objects = newSelection.ToArray();
            Undo.RecordObjects(Selection.objects, "Selection Change");
        }
    }

    /// <summary>
    /// sets the RectTransform's anchors to match it's bounds
    /// </summary>
    [MenuItem("GameObject/Invert Selection", false, 45)]
    public static void EditInvertSelectionOnHierarchyLevel()
    {
        InvertSelectionOnHierarchyLevel();
    }

    /// <summary>
    /// applies changes made to all prefabs in current selection (NOT Undoable)
    /// </summary>
    [MenuItem(Menu + "Apply All Prefabs")]
    public static void ApplyAllPrefabs()
    {
        GameObject[] selection = Selection.gameObjects;

        for (int i = 0; i < selection.Length; i++)
        {
            GameObject sel = selection[i];
            PrefabType pType = PrefabUtility.GetPrefabType(sel);
            if (pType == PrefabType.PrefabInstance)
            {
                PrefabUtility.ReplacePrefab(sel, PrefabUtility.GetPrefabParent(sel), ReplacePrefabOptions.ConnectToPrefab);
            }

            EditorUtility.DisplayCancelableProgressBar(typeof(ContextExtensions).Name, "Applying changes for '" + sel.name + "'.", ((float)i / (float)selection.Length));
        }
        EditorUtility.ClearProgressBar();
    }

    /// <summary>
    /// removes empty asset folders inside the selected folder in the hierarchy view
    /// </summary>
    [MenuItem(Menu + "Remove empty folders (Selection) #&c")]
    public static void RemoveEmptyFoldersInSelection() //List<string> list)
    {
        List<string> folders = new List<string>();

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("Remove empty folders:");
        foreach (UnityEngine.Object sel in Selection.objects)
        {
            string assetPath = AssetDatabase.GetAssetPath(sel);
            if (AssetDatabase.IsValidFolder(assetPath))
            {
                folders.Add(assetPath);
                folders.AddRange(GetAllSubFolders(assetPath));
            }
        }
        folders = folders.Distinct().ToList();
        // add assets from nested asset paths & folders
        folders.Sort();
        folders.Reverse();

        for (int i = 0; i < folders.Count; i++)
        {
            if (Directory.GetFiles(folders[i]).Length > 0
                || Directory.GetDirectories(folders[i]).Length > 0)
                continue;
            else
            {
                AssetDatabase.MoveAssetToTrash(folders[i]);
                sb.AppendLine("Removed: " + folders[i]);
            }
        }
        Debug.Log(sb.ToString());
        //Debug.Log(string.Format("Found {0} {1}(s) in selection.", occurrences.Count, typeof(T).Name));
        EditorUtility.ClearProgressBar();
    }
    private static string[] GetAllSubFolders(string path)
    {
        return Directory.GetDirectories(path, "*", SearchOption.AllDirectories);
    }

    /// <summary>
    /// prints a list of all selected objects to the console (might get truncated when too long)
    /// </summary>
    [MenuItem(Menu + "List Selected Objects", false)]
    public static void ListSelectedObjects()
    {
        string result = "";
        foreach (UnityEngine.Object sel in Selection.objects)
            result += sel.name + Environment.NewLine;
        Debug.Log(result);
    }

    /// <summary>
    /// removes the parenthesis part of the selected objects name, e.g. "MyObject (clone)" => "MyObject"
    /// </summary>
    /// <remarks>This method also removes trailing whitespaces after parenthesis removal</remarks>
    [MenuItem(Menu + "Trim name to parenthesis (from end)")]
    public static void TrimParenthesisFromEnd()
    {
        foreach (GameObject go in Selection.gameObjects)
        {
            Undo.RegisterFullObjectHierarchyUndo(go, "Trim name to parenthesis");
            RemoveParenthesisFromEnd(go.transform);
        }
    }
    private static void RemoveParenthesisFromEnd(Transform transform)
    {
        string name = transform.name;
        int indLP = name.LastIndexOf('(');

        if (indLP > 0)
            transform.name = name.Substring(0, indLP).Trim();

        for (int i = 0; i < transform.childCount; i++)
            RemoveParenthesisFromEnd(transform.GetChild(i));
    }

    /// <summary>
    /// Rescales/-positions all sprites on a texture by the given values
    /// </summary>
    [MenuItem(Menu + "Resize Sprites on Texture2D asset")]
    public static void RescaleSpritesToTrimmedTexture()
    {
        ScriptableWizard.DisplayWizard("Resize Sprites", typeof(ModifySpriteRectanglesWizard), "Resize");

        /*
        if (!Selection.activeObject.GetType().Equals(typeof(Texture2D)))
            return;

        Texture2D texAsset = (Texture2D)AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GetAssetPath(Selection.activeObject));

        string path = AssetDatabase.GetAssetPath(texAsset);
        TextureImporter ti = AssetImporter.GetAtPath(path) as TextureImporter;
        ti.isReadable = true;

        int shiftX = 0; // shift on the horizontal axis ('+' = right, '-' = left)
        int shiftY = -512; // shift on the vertical axis ('+' = up, '-' = down)
        float scaleX = 1.0f; // scale of each sprite to match the new texture size/ratio/resolution
        float scaleY = 1.0f; // scale of each sprite to match the new texture size/ratio/resolution

        SpriteMetaData[] spriteSheet = ti.spritesheet;
        for (int i = 0; i < spriteSheet.Length; i++)
        {
            Rect rect = spriteSheet[i].rect;
            rect.x += shiftX;
            rect.y += shiftY;
            rect.width *= scaleX;
            rect.height *= scaleY;
            spriteSheet[i].rect = rect;
        }

        ti.spritesheet = spriteSheet;
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        */
    }

    public class ModifySpriteRectanglesWizard : ScriptableWizard
    {
        public static Rect ResizeRect = new Rect(0, 0, 1, 1);

        public Rect _resizeRect = new Rect();

        //[NoXP.QuickAccess.BuiltIn("Replace Selection", "NoXP.Extentions.ReplaceSelectionWizard")]
        //public static void CreateWizard()
        //{
        //    ScriptableWizard.DisplayWizard("Resize Sprites", typeof(ModifySpriteRectanglesWizard), "Resize");
        //}

        public ModifySpriteRectanglesWizard()
        {
            _resizeRect = ResizeRect;
            this.maxSize = new Vector2(this.maxSize.x, 160);
        }


        void OnWizardUpdate()
        {
            ResizeRect = _resizeRect;
        }

        void OnWizardCreate()
        {
            if (Selection.activeObject == null
                || !Selection.activeObject.GetType().Equals(typeof(Texture2D)))
                return;

            Texture2D texAsset = (Texture2D)AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GetAssetPath(Selection.activeObject));
            string path = AssetDatabase.GetAssetPath(texAsset);
            TextureImporter ti = AssetImporter.GetAtPath(path) as TextureImporter;
            ti.isReadable = true;

            SpriteMetaData[] spriteSheet = ti.spritesheet;
            for (int i = 0; i < spriteSheet.Length; i++)
            {
                Rect rect = spriteSheet[i].rect;
                rect.x += ResizeRect.x;
                rect.y += ResizeRect.y;
                rect.width *= ResizeRect.width;
                rect.height *= ResizeRect.height;
                spriteSheet[i].rect = rect;
            }

            ti.spritesheet = spriteSheet;
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        }
    }

    /// <summary>
    /// sets the RectTransform's anchors to match it's bounds
    /// </summary>
    [MenuItem("CONTEXT/RectTransform/Anchors to Rect")]
    public static void SetAnchorToLocationAll()
    {
        foreach (GameObject sGo in Selection.gameObjects)
            SetAnchorToLocation(sGo.GetComponent<RectTransform>());
    }
    private static void SetAnchorToLocation(RectTransform rectTfs)
    {
        if (rectTfs != null && rectTfs.parent != null)
        {
            Undo.RecordObject(rectTfs, "Anchors to Rect");
            RectTransform parent = (RectTransform)rectTfs.parent;


            float oWidth = rectTfs.rect.width;
            float oHeight = rectTfs.rect.height;

            float relWidth = rectTfs.rect.width / parent.rect.width;
            float relHeight = rectTfs.rect.height / parent.rect.height;

            float relX = rectTfs.offsetMin.x / parent.rect.width + rectTfs.anchorMin.x;
            float relY = rectTfs.offsetMin.y / parent.rect.height + rectTfs.anchorMin.y;

            rectTfs.anchorMin = new Vector2(relX, relY);
            rectTfs.anchorMax = new Vector2(relX + relWidth, relY + relHeight);

            rectTfs.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, oWidth);
            rectTfs.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, oHeight);
            rectTfs.anchoredPosition = Vector2.zero;

            //Debug.Log(string.Format("{0}, {1}, {2}, {3}", relX, relY, relX + relWidth, relY + relHeight));
        }

    }

    /// <summary>
    /// moves the current component to the top of the component stack
    /// </summary>
    [MenuItem("CONTEXT/Behaviour/Move Up Most", false)]
    public static void MoveUpMost()
    {
        int numOfComps = Selection.activeGameObject.GetComponents<Component>().Length;
        //Debug.Log(numOfComps);
        for (int i = 0; i < numOfComps; i++)
            EditorApplication.ExecuteMenuItem("CONTEXT/Component/Move Up");
    }

    /// <summary>
    /// moves the current component to the bottom of the component stack
    /// </summary>
    [MenuItem("CONTEXT/Behaviour/Move Down Most", false)]
    public static void MoveDownMost()
    {
        int numOfComps = Selection.activeGameObject.GetComponents<Component>().Length;
        //Debug.Log(numOfComps);
        for (int i = 0; i < numOfComps; i++)
            EditorApplication.ExecuteMenuItem("CONTEXT/Component/Move Down");
    }

}