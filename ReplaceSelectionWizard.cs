﻿/* This wizard will replace a selection with an object or prefab.
 * Scene objects will be cloned (destroying their prefab links).
 * Original coding by 'yesfish', nabbed from Unity Forums
 * 'keep parent' added by Dave A (also removed 'rotation' option, using localRotation
 * added undo queue registration by Tobias Pott (www.tobiaspott.de)
 */

using UnityEngine;
using UnityEditor;

public class ReplaceSelectionWizard : ScriptableWizard
{
    static GameObject _replacement = null;
    static bool _keep = false;

    public GameObject ReplacementObject = null;
    public bool KeepOriginals = false;

    //[NoXP.QuickAccess.BuiltIn("Replace Selection", "NoXP.Extentions.ReplaceSelectionWizard")]
    [MenuItem("GameObject/Replace Selection... #&r")]
    public static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard(
            "Replace Selection", typeof(ReplaceSelectionWizard), "Replace");
    }

    public ReplaceSelectionWizard()
    {
        ReplacementObject = _replacement;
        KeepOriginals = _keep;
    }


    void OnWizardUpdate()
    {
        _replacement = ReplacementObject;
        _keep = KeepOriginals;
    }

    void OnWizardCreate()
    {
        if (_replacement == null)
            return;

        Transform[] transforms = Selection.GetTransforms(
            SelectionMode.TopLevel | SelectionMode.OnlyUserModifiable);

        Undo.RecordObjects(transforms, "Replace Selection");

        foreach (Transform t in transforms)
        {
            GameObject g;
            PrefabType pref = PrefabUtility.GetPrefabType(_replacement);

            if (pref == PrefabType.Prefab || pref == PrefabType.ModelPrefab)
            {
                g = (GameObject)PrefabUtility.InstantiatePrefab(_replacement);
                Undo.RegisterCreatedObjectUndo(g, "Replace Selection");
            }
            else
            {
                g = (GameObject)Editor.Instantiate(_replacement);
                Undo.RegisterCreatedObjectUndo(g, "Replace Selection");
            }

            g.SetActive(t.gameObject.activeSelf);

            Transform gTransform = g.transform;
            gTransform.parent = t.parent;
            g.name = _replacement.name;
            gTransform.position = t.position;
            gTransform.localScale = t.localScale;
            gTransform.rotation = t.rotation;
        }

        if (!_keep)
        {
            foreach (GameObject g in Selection.gameObjects)
                Undo.DestroyObjectImmediate(g);
        }
    }
}